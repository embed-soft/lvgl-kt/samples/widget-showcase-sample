package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MAJOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MINOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_PATCH
import io.gitlab.embedSoft.lvglKt.core.runEventLoop
import org.example.widgetShowcase.view.loadMainScreen

fun main() {
    println("Starting Widget Showcase...")
    initSubSystems()
    loadMainScreen()
    println("Kotlin Version: ${KotlinVersion.CURRENT}")
    println("LVGL Version: ${LVGL_VERSION_MAJOR}.${LVGL_VERSION_MINOR}.${LVGL_VERSION_PATCH}")
    runEventLoop()
}
