package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.initLvgl
import io.gitlab.embedSoft.lvglKt.sdl2.Sdl2
import kotlin.system.exitProcess

internal fun initSubSystems() {
    initLvgl()
    Sdl2.initHal(true)
}

internal actual fun exitProgram(exitCode: Int) {
    exitProcess(exitCode)
}
