package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.percent

internal actual val mainLayoutSizes: Map<String, Pair<Short, Short>> = mapOf(
    "arc" to (28.toShort().percent to 42.toShort().percent),
    "bar" to (30.toShort().percent to 30.toShort().percent),
    "buttonMatrix" to (35.toShort().percent to 40.toShort().percent),
    "button" to (15.toShort().percent to 30.toShort().percent),
    "checkbox" to (15.toShort().percent to 40.toShort().percent),
    "dropDownList" to (20.toShort().percent to 28.toShort().percent),
    "label" to (30.toShort().percent to 40.toShort().percent),
    "line" to (30.toShort().percent to 35.toShort().percent),
    "roller" to (30.toShort().percent to 40.toShort().percent),
    "slider" to (30.toShort().percent to 40.toShort().percent),
    "switch" to (15.toShort().percent to 45.toShort().percent),
    "table" to (35.toShort().percent to 50.toShort().percent),
    "textArea" to (30.toShort().percent to 30.toShort().percent),
    "main" to (95.toShort().percent to 35.toShort().percent)
)
