package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.arc
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var arcScreen: Screen? = null

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["arc"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Arc")
    createBackButton(true, this)
    createArc(this)
}

private fun createArc(parent: LvglObjectBase) = arc(parent = parent) {
    setRotation(135u)
    setBackgroundAngles(start = 0u, end = 270u)
    value = 40
    setSize(width = 150, height = 150)
}

internal fun loadArcScreen() {
    arcScreen = createScreen()
    Screen.load(arcScreen!!)
    closeMainScreen()
}

internal fun closeArcScreen() {
    arcScreen?.close()
    arcScreen = null
}
