package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.point
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.core.styling.mainPaletteColor
import io.gitlab.embedSoft.lvglKt.core.styling.style
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.line
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var screen: Screen? = null
private val points = arrayOf(
    point(5, 5),
    point(70, 70),
    point(120, 10),
    point(180, 60),
    point(240, 10)
)

private fun createStyle() = style {
    setLineWidth(8)
    setLineColor(mainPaletteColor(5u))
    setLineRounded(true)
}

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["line"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Line")
    createBackButton(true, this)
    createLine(this)
}

private fun createLine(parent: LvglObjectBase) = line(parent = parent) {
    setPoints(*points)
    addStyle(createStyle())
}

internal fun loadLineScreen() {
    screen = createScreen()
    Screen.load(screen!!)
    closeMainScreen()
}

internal fun closeLineScreen() {
    screen?.close()
    screen = null
}
