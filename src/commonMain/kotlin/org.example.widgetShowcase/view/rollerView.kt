package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.RollerMode
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asRoller
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.roller
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var rollerScreen: Screen? = null
private var callback: LvglEventCallback? = null
private val rollerOptions = arrayOf(
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
)

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        val target = evt.target.asRoller()
        println("Selected month: ${target.getSelectedString()}")
    }
}

private fun createRollerScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["roller"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Roller")
    createBackButton(true, this)
    createRoller(this)
}

private fun createRoller(parent: LvglObjectBase) = roller(parent = parent) {
    setOptions(RollerMode.INFINITE, *rollerOptions)
    setVisibleRowCount(3u)
    addEventCallback(LvglEventType.ALL.value, callback!!)
    width = 60.toShort().percent
}

internal fun loadRollerScreen() {
    callback = createCallback()
    rollerScreen = createRollerScreen()
    Screen.load(rollerScreen!!)
    closeMainScreen()
}

internal fun closeRollerScreen() {
    rollerScreen?.close()
    callback?.close()
    rollerScreen = null
    callback = null
}
