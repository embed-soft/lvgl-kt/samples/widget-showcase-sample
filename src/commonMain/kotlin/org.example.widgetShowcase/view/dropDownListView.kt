package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asDropDownList
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.dropDownList
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var screen: Screen? = null
private val items = arrayOf(
    "Apple",
    "Banana",
    "Orange",
    "Cherry",
    "Grape",
    "Raspberry",
    "Melon",
    "Orange",
    "Lemon",
    "Nuts"
)
private var callback: LvglEventCallback? = null

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    val target = evt.target.asDropDownList()
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        println("Option: ${target.selectedOption}")
    }
}

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["dropDownList"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Drop Down List")
    createBackButton(true, this)
    createDropDownList(this)
}

private fun createDropDownList(parent: LvglObjectBase) = dropDownList(parent = parent) {
    options = items
    addEventCallback(LvglEventType.ALL, callback!!)
}

internal fun loadDropDownListScreen() {
    callback = createCallback()
    screen = createScreen()
    Screen.load(screen!!)
    closeMainScreen()
}

internal fun closeDropDownListScreen() {
    screen?.close()
    callback?.close()
    callback = null
    screen = null
}
