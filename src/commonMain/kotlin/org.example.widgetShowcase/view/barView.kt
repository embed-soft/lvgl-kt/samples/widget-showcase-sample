package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.bar
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var barScreen: Screen? = null

private fun createBarScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["bar"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(parent = this, newText = "Bar")
    createBackButton(vertLayout = true, parent = this)
    createBar(this)
}

private fun createBar(parent: LvglObjectBase) = bar(parent) {
    setSize(width = 200, height = 20)
    center()
    setValue(70, false)
}

internal fun loadBarScreen() {
    barScreen = createBarScreen()
    Screen.load(barScreen!!)
    closeMainScreen()
}

internal fun closeBarScreen() {
    barScreen?.close()
    barScreen = null
}
