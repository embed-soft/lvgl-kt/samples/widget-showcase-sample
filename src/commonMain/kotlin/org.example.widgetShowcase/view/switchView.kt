package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglState
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.lvglObject.value
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asSwitch
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.switch
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes
import kotlin.experimental.or

private var switchScreen: Screen? = null
private var callback: LvglEventCallback? = null

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        val target = evt.target.asSwitch()
        println("State: ${if (target.hasState(LvglState.CHECKED)) "On" else "Off"}")
    }
}

private fun createSwitchScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["switch"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Switch")
    createBackButton(true, this)
    createSwitches(this)
}

private fun createSwitches(parent: LvglObjectBase) {
    switch(parent) { addEventCallback(LvglEventType.ALL.value, callback!!) }
    switch(parent) {
        addState(LvglState.CHECKED)
        addEventCallback(LvglEventType.ALL.value, callback!!)
    }
    switch(parent) {
        addState(LvglState.DISABLED)
        addEventCallback(LvglEventType.ALL.value, callback!!)
    }
    switch(parent) {
        val tmpChecked = LvglState.CHECKED.value.toByte()
        val tmpDisabled = LvglState.DISABLED.value.toByte()
        addState((tmpChecked or tmpDisabled).toUShort())
        addEventCallback(LvglEventType.ALL.value, callback!!)
    }
}

internal fun loadSwitchScreen() {
    callback = createCallback()
    switchScreen = createSwitchScreen()
    Screen.load(switchScreen!!)
    closeMainScreen()
}

internal fun closeSwitchScreen() {
    switchScreen?.close()
    callback?.close()
    switchScreen = null
    callback = null
}
