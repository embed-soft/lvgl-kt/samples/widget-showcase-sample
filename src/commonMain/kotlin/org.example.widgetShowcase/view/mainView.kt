package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.layout.FlexFlow
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.button
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.mainLayoutSizes

private lateinit var mainScreen: Screen
private var items: Array<Pair<String, LvglEventCallback>>? = null

private fun createItems() = arrayOf(
    "Bar" to LvglEventCallback.create { _, _ -> loadBarScreen() },
    "Button" to LvglEventCallback.create { _, _ -> loadButtonScreen() },
    "Checkbox" to LvglEventCallback.create { _, _ -> loadCheckboxScreen() },
    "Label" to LvglEventCallback.create { _, _ -> loadLabelScreen() },
    "Roller" to LvglEventCallback.create { _, _ -> loadRollerScreen() },
    "Slider" to LvglEventCallback.create { _, _ -> loadSliderScreen() },
    "Switch" to LvglEventCallback.create { _, _ -> loadSwitchScreen() },
    "Text Area" to LvglEventCallback.create { _, _ -> loadTextAreaScreen() },
    "Arc" to LvglEventCallback.create { _, _ -> loadArcScreen() },
    "Button Matrix" to LvglEventCallback.create { _, _ -> loadButtonMatrixScreen() },
    "Drop Down List" to LvglEventCallback.create { _, _ -> loadDropDownListScreen() },
    "Line" to LvglEventCallback.create { _, _ -> loadLineScreen() },
    "Table" to LvglEventCallback.create { _, _ -> loadTableScreen() }
)

private fun createMainScreen() = screen { createMainLayout(this) }

private fun createLabel(parent: LvglObjectBase, newText: String) = label(parent) {
    text = newText
    center()
}

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["main"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(newWidth = width, newHeight = height, flowType = FlexFlow.ROW_WRAP)
    items?.forEach { (text, callback) ->
        createButton(parent = this, newText = text, callback = callback)
    }
}

private fun createButton(
    parent: LvglObjectBase,
    newText: String,
    callback: LvglEventCallback
) = button(parent = parent) {
    createLabel(this, newText)
    setSize(width = 15.toShort().percent, height = 32.toShort().percent)
    addEventCallback(LvglEventType.CLICKED.value, callback)
}

internal fun loadMainScreen() {
    items?.forEach { (_, callback) -> callback.close() }
    items = null
    items = createItems()
    mainScreen = createMainScreen()
    Screen.load(mainScreen)
    closeOtherScreens()
}

private fun closeOtherScreens() {
    closeBarScreen()
    closeButtonScreen()
    closeCheckboxScreen()
    closeLabelScreen()
    closeRollerScreen()
    closeSliderScreen()
    closeSwitchScreen()
    closeTextAreaScreen()
    closeArcScreen()
    closeButtonMatrixScreen()
    closeDropDownListScreen()
    closeLineScreen()
    closeTableScreen()
}

internal fun closeMainScreen() {
    mainScreen.close()
}
