package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglAlignment
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.lvglObject.value
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.core.styling.setBorderWidthStyle
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asSlider
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.slider
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.Label
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var sliderScreen: Screen? = null
private var sliderLbl: Label? = null
private var callback: LvglEventCallback? = null

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        val target = evt.target.asSlider()
        sliderLbl?.text = "${target.getValue()}%"
        sliderLbl?.alignTo(base = target, align = LvglAlignment.BOTTOM_MID.value.toUByte(), xOffset = 0, yOffset = 30)
    }
}

private fun createSliderScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["slider"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Slider")
    createBackButton(true, this)
    createSliderGroup(this)
}

private fun createSlider(parent: LvglObjectBase) = slider(parent = parent) {
    width = 90.toShort().percent
    addEventCallback(LvglEventType.VALUE_CHANGED.value, callback!!)
}

private fun createSliderGroup(parent: LvglObjectBase) = lvglObject(parent) {
    setBorderWidthStyle(0)
    width = 100.toShort().percent
    val slider = createSlider(this)
    sliderLbl = createSliderLabel(this, slider)
}

private fun createSliderLabel(parent: LvglObjectBase, alignBase: LvglObjectBase) = label(parent) {
    text = "0%"
    alignTo(base = alignBase, align = LvglAlignment.BOTTOM_MID.value.toUByte(), xOffset = 0, yOffset = 30)
}

internal fun loadSliderScreen() {
    callback = createCallback()
    sliderScreen = createSliderScreen()
    Screen.load(sliderScreen!!)
    closeMainScreen()
}

internal fun closeSliderScreen() {
    sliderScreen?.close()
    callback?.close()
    callback = null
    sliderScreen = null
    sliderLbl = null
}
