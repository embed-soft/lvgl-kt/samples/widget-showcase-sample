package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.core.styling.TextAlignment
import io.gitlab.embedSoft.lvglKt.core.styling.setTextAlignStyle
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.LabelLongMode
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var screen: Screen? = null

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["label"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Label")
    createBackButton(true, this)
    createLabel1(this)
    createLabel2(this)
}

private fun createLabel2(parent: LvglObjectBase) = label(parent = parent) {
    longMode = LabelLongMode.SCROLL_CIRCULAR
    width = 40.toShort().percent
    text = "It is a circularly scrolling text. "
}

private fun createLabel1(parent: LvglObjectBase) = label(parent = parent) {
    // Break the long lines.
    longMode = LabelLongMode.WRAP
    // Enable re-coloring by commands in the text.
    recolor = true
    text = "#0000ff Re-color# #ff00ff words# #ff0000 of a# label, align the lines to the center \n" +
        "and wrap long text automatically."
    // Set smaller width to make the lines wrap.
    width = 100.toShort().percent
    setTextAlignStyle(TextAlignment.CENTER)
}

internal fun loadLabelScreen() {
    screen = createScreen()
    Screen.load(screen!!)
    closeMainScreen()
}

internal fun closeLabelScreen() {
    screen?.close()
    screen = null
}
