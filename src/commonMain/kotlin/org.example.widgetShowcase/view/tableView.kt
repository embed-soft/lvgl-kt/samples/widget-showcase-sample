package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.getParamAsLvglObjectDrawPartDescriptor
import io.gitlab.embedSoft.lvglKt.core.lvglObject.*
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.core.styling.Opacity
import io.gitlab.embedSoft.lvglKt.core.styling.Palette
import io.gitlab.embedSoft.lvglKt.core.styling.TextAlignment
import io.gitlab.embedSoft.lvglKt.core.styling.mainPaletteColor
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.Table
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asTable
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.table
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var screen: Screen? = null
private var callback: LvglEventCallback? = null
private val names = arrayOf(
    "Name",
    "Apple",
    "Banana",
    "Lemon",
    "Grape",
    "Melon",
    "Peach",
    "Nuts"
)
private val prices = arrayOf(
    "Price",
    "$7",
    "$4",
    "$6",
    "$2",
    "$5",
    "$1",
    "$9"
)

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    val target = evt.target.asTable()
    val descriptor = evt.getParamAsLvglObjectDrawPartDescriptor()
    // Check if the cell is drawn.
    if (descriptor.part == LvglPart.ITEMS.value) {
        val row = descriptor.id / target.colCount
        val col = descriptor.id - row * target.colCount
        if (row == 0u) descriptor.styleFirstRow(col)
        if ((row % 2u == 0u) && row != 0u) descriptor.styleEvenRow()
    }
}

private fun LvglObjectDrawPartDescriptor.styleFirstRow(col: UInt) {
    // Make the text in the first row center aligned.
    labelDescriptor.align = TextAlignment.CENTER.value
    if (col == 0u) {
        rectDescriptor.backgroundColor = mainPaletteColor(Palette.LIGHT_BLUE)
    } else {
        rectDescriptor.backgroundColor =
            mainPaletteColor(Palette.BLUE).mix(rectDescriptor.backgroundColor, Opacity.TWENTY)
    }
    rectDescriptor.backgroundOpacity = Opacity.ONE_HUNDRED.value
}

private fun LvglObjectDrawPartDescriptor.styleEvenRow() {
    // Make every 2nd row grayish.
    rectDescriptor.backgroundColor = mainPaletteColor(Palette.GREY).mix(rectDescriptor.backgroundColor, Opacity.TEN)
    rectDescriptor.backgroundOpacity = Opacity.ONE_HUNDRED.value
}

private fun Table.addNameCells() {
    names.forEachIndexed { pos, item ->
        setCellValue(row = pos.toUShort(), col = 0u, txt = item)
    }
}

private fun Table.addPriceCells() {
    prices.forEachIndexed { pos, item ->
        setCellValue(row = pos.toUShort(), col = 1u, txt = item)
    }
}

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["table"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Table")
    createBackButton(true, this)
    createTable(this)
}

private fun createTable(parent: LvglObjectBase) = table(parent = parent) {
    // Set a smaller height to the table. It'll make it scrollable.
    height = 70.toShort().percent
    addNameCells()
    addPriceCells()
    addEventCallback(LvglEventType.DRAW_PART_BEGIN, callback!!)
}

internal fun loadTableScreen() {
    callback = createCallback()
    screen = createScreen()
    Screen.load(screen!!)
    closeMainScreen()
}

internal fun closeTableScreen() {
    screen?.close()
    callback?.close()
    callback = null
    screen = null
}
