package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.ButtonMatrixControl
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asButtonMatrix
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.buttonMatrix
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var buttonMatrixScreen: Screen? = null
private val btnLabels = arrayOf("1", "2", "3", "4", "5", "\n", "6", "7", "8", "9", "0", "\n", "Action 1", "Action 2")
private var callback: LvglEventCallback? = null

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    val target = evt.target.asButtonMatrix()
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        val id = target.selectedButton
        println("${target.getButtonText(id)} was pressed.")
    }
}

private fun createScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["buttonMatrix"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Button Matrix")
    createBackButton(true, this)
    createButtonMatrix(this)
}

private fun createButtonMatrix(parent: LvglObjectBase) = buttonMatrix(parent = parent) {
    map = btnLabels
    // Make "Action 1" twice as wide as "Action 2".
    setButtonWidth(10u, 2u)
    setButtonControl(10u, ButtonMatrixControl.CHECKABLE)
    setButtonControl(11u, ButtonMatrixControl.CHECKED)
    addEventCallback(LvglEventType.ALL, callback!!)
}

internal fun loadButtonMatrixScreen() {
    callback = createCallback()
    buttonMatrixScreen = createScreen()
    Screen.load(buttonMatrixScreen!!)
    closeMainScreen()
}

internal fun closeButtonMatrixScreen() {
    buttonMatrixScreen?.close()
    callback?.close()
    callback = null
    buttonMatrixScreen = null
}
