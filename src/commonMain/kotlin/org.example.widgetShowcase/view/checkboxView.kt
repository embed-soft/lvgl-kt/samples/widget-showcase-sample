package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglState
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asCheckbox
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.checkbox
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var checkboxScreen: Screen? = null
private var callback: LvglEventCallback? = null
private val items = arrayOf("Apple" to false, "Banana" to true, "Lemon" to false, "Melon" to true)

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    val target = evt.target.asCheckbox()
    if (evt.code == LvglEventType.VALUE_CHANGED) {
        val checkedTxt = if (target.hasState(LvglState.CHECKED)) "checked" else "unchecked"
        println("The ${target.text} checkbox is now $checkedTxt.")
    }
}

private fun createCheckboxScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["checkbox"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Checkbox")
    createBackButton(true, this)
    items.forEach { (text, checked) ->
        createCheckbox(parent = this, newText = text, checked = checked)
    }
}

internal fun loadCheckboxScreen() {
    callback = createCallback()
    checkboxScreen = createCheckboxScreen()
    Screen.load(checkboxScreen!!)
    closeMainScreen()
}

internal fun closeCheckboxScreen() {
    checkboxScreen?.close()
    callback?.close()
    callback = null
    checkboxScreen = null
}

private fun createCheckbox(parent: LvglObjectBase, newText: String, checked: Boolean = false) = checkbox(parent) {
    text = newText
    addEventCallback(LvglEventType.ALL.value, callback!!)
    if (checked) addState(LvglState.CHECKED)
}
