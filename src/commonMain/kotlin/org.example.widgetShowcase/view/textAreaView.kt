package org.example.widgetShowcase.view

internal expect fun loadTextAreaScreen()

internal expect fun closeTextAreaScreen()
