package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.button
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label
import org.example.widgetShowcase.applyMainLayoutStyling
import org.example.widgetShowcase.createBackButton
import org.example.widgetShowcase.createHeaderLabel
import org.example.widgetShowcase.mainLayoutSizes

private var buttonScreen: Screen? = null

private fun createButtonScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["button"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Button")
    createBackButton(true, this)
    createButton(this)
}

private fun createButton(parent: LvglObjectBase) = button(parent = parent) {
    label(this) {
        text = "Button"
        center()
    }
    setSize(width = 100.toShort().percent, height = 50)
}

internal fun loadButtonScreen() {
    buttonScreen = createButtonScreen()
    Screen.load(buttonScreen!!)
    closeMainScreen()
}

internal fun closeButtonScreen() {
    buttonScreen?.close()
    buttonScreen = null
}
