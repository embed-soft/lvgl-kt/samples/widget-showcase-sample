package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.layout.FlexFlow
import io.gitlab.embedSoft.lvglKt.core.layout.setFlexFlow
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglAlignment
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObject
import io.gitlab.embedSoft.lvglKt.core.lvglObject.value

private const val MAIN_LAYOUT_X_OFFSET = 10.toShort()
private const val MAIN_LAYOUT_Y_OFFSET = 10.toShort()
private val mainLayoutAlignType = LvglAlignment.TOP_LEFT.value.toUByte()
internal expect val mainLayoutSizes: Map<String, Pair<Short, Short>>

internal fun LvglObject.applyMainLayoutStyling(
    newWidth: Short,
    newHeight: Short,
    flowType: FlexFlow = FlexFlow.COLUMN
) {
    align(align = mainLayoutAlignType, xOffset = MAIN_LAYOUT_X_OFFSET, yOffset = MAIN_LAYOUT_Y_OFFSET)
    setFlexFlow(flowType)
    width = newWidth
    height = newHeight
}
