package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.percent
import io.gitlab.embedSoft.lvglKt.core.styling.mainPaletteColor
import io.gitlab.embedSoft.lvglKt.core.styling.style
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.button
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label
import org.example.widgetShowcase.view.loadMainScreen

private val backBtnCallback = LvglEventCallback.create { _, _ -> loadMainScreen() }
internal expect val vertLayoutBtnWidth: Short

private fun createBackBtnStyle(vertLayout: Boolean) = style {
    val tealColor = 7u
    setBorderWidth(3)
    setBackgroundColor(mainPaletteColor(tealColor))
    if (vertLayout) {
        setWidth(vertLayoutBtnWidth)
        setHeight(50)
    } else {
        setWidth(50)
        setHeight(100.toShort().percent)
    }
}

private fun createHeaderLabelStyle() = style {
    setPadBottom(30)
    setWidth(100.toShort().percent)
}

internal fun createHeaderLabel(parent: LvglObjectBase, newText: String) = label(parent) {
    addStyle(createHeaderLabelStyle())
    text = newText
}

internal fun createBackButton(vertLayout: Boolean, parent: LvglObjectBase) = button(parent = parent) {
    addStyle(createBackBtnStyle(vertLayout))
    label(this) {
        text = "Back"
        center()
    }
    addEventCallback(LvglEventType.CLICKED.value, backBtnCallback)
}
