package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.percent

internal actual val vertLayoutBtnWidth: Short = 20.toShort().percent
