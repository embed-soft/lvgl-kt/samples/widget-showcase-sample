package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.percent

val mainLayoutWidth: Short = 100.toShort().percent
val mainLayoutHeight: Short = 100.toShort().percent
internal actual val mainLayoutSizes: Map<String, Pair<Short, Short>> = mapOf(
    "arc" to (mainLayoutWidth to mainLayoutHeight),
    "bar" to (mainLayoutWidth to mainLayoutHeight),
    "buttonMatrix" to (mainLayoutWidth to mainLayoutHeight),
    "button" to (mainLayoutWidth to mainLayoutHeight),
    "checkbox" to (mainLayoutWidth to mainLayoutHeight),
    "dropDownList" to (mainLayoutWidth to mainLayoutHeight),
    "label" to (mainLayoutWidth to mainLayoutHeight),
    "line" to (mainLayoutWidth to mainLayoutHeight),
    "roller" to (mainLayoutWidth to mainLayoutHeight),
    "slider" to (mainLayoutWidth to mainLayoutHeight),
    "switch" to (mainLayoutWidth to mainLayoutHeight),
    "table" to (mainLayoutWidth to mainLayoutHeight),
    "textArea" to (mainLayoutWidth to mainLayoutHeight),
    "main" to (mainLayoutWidth to mainLayoutHeight)
)
