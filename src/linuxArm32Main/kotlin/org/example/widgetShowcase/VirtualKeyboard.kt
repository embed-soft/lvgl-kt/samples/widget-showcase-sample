package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.TextArea
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

internal class VirtualKeyboard private constructor(parent: LvglObjectBase): LvglObjectBase {
    override val lvObjPtr: CPointer<lv_obj_t>? = lv_keyboard_create(parent.lvObjPtr)

    var textArea: TextArea
        get() = TextArea.fromPointer(lv_keyboard_get_textarea(lvObjPtr)?.reinterpret())
        set(value) {
            lv_keyboard_set_textarea(lvObjPtr, value.lvObjPtr)
        }

    fun hide() {
        lv_obj_add_flag(lvObjPtr, LV_OBJ_FLAG_HIDDEN)
    }

    fun show() {
        lv_obj_clear_flag(lvObjPtr, LV_OBJ_FLAG_HIDDEN)
    }

    companion object {
        fun create(parent: LvglObjectBase): VirtualKeyboard = VirtualKeyboard(parent)
    }
}
