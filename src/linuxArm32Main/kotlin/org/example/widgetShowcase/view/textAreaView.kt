package org.example.widgetShowcase.view

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.*
import io.gitlab.embedSoft.lvglKt.core.screen
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.asTextArea
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.textArea
import org.example.widgetShowcase.*

private var textAreaScreen: Screen? = null
private var callback: LvglEventCallback? = null
private var keyboard: VirtualKeyboard? = null

private fun createCallback() = LvglEventCallback.create { evt, _ ->
    val target = evt.target.asTextArea()
    val code = evt.code
    @Suppress("CascadeIf")
    if (code == LvglEventType.READY) {
        println("Enter was pressed. The current text is: ${target.text}")
        keyboard?.hide()
    } else if (code == LvglEventType.FOCUSED) {
        keyboard?.show()
    } else if (code == LvglEventType.DEFOCUSED) {
        keyboard?.hide()
    } else if (code == LvglEventType.CLICKED) {
        keyboard?.show()
    }
}

private fun createTextAreaScreen() = screen { createMainLayout(this) }

private fun createMainLayout(parent: LvglObjectBase) = lvglObject(parent) {
    val (width, height) = mainLayoutSizes["textArea"]
        ?: throw IllegalStateException("Cannot obtain width and height for mainLayout")
    applyMainLayoutStyling(width, height)
    createHeaderLabel(this, "Text Area")
    createBackButton(true, this)
    val txtArea = createTextArea(this)
    keyboard = VirtualKeyboard.create(this)
    keyboard?.textArea = txtArea
}

private fun createTextArea(parent: LvglObjectBase) = textArea(parent = parent) {
    oneLine = true
    // Ensure the cursor is visible.
    addState(LvglState.FOCUSED)
    addEventCallback(LvglEventType.ALL.value, callback!!)
}

internal actual fun loadTextAreaScreen() {
    callback = createCallback()
    textAreaScreen = createTextAreaScreen()
    Screen.load(textAreaScreen!!)
    closeMainScreen()
}

internal actual fun closeTextAreaScreen() {
    textAreaScreen?.close()
    callback?.close()
    textAreaScreen = null
    callback = null
    keyboard = null
}
