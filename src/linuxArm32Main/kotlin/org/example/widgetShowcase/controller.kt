package org.example.widgetShowcase

import io.gitlab.embedSoft.lvglKt.core.initLvgl
import io.gitlab.embedSoft.lvglKt.drivers.input.initEvdev
import io.gitlab.embedSoft.lvglKt.frameBuffer.FrameBuffer
import kotlin.system.exitProcess

internal fun initSubSystems() {
    initLvgl()
    FrameBuffer.open()
    initEvdev()
}

internal actual fun exitProgram(exitCode: Int) {
    exitProcess(exitCode)
}
