group = "org.embedsoft"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.7.21"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val lvglKtVer = "0.4.0"
    val programEntryPoint = "org.example.widgetShowcase.main"
    val embedSoftGroupId = "io.gitlab.embed-soft"
    val coreWidgetsDependency = "$embedSoftGroupId:lvglkt-core-widgets:$lvglKtVer"
    val binaryName = "widget_showcase"

    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                implementation(coreWidgetsDependency)
                implementation("$embedSoftGroupId:lvglkt-sdl2:$lvglKtVer")
            }
        }

        binaries {
            executable(binaryName) {
                entryPoint = programEntryPoint
                linkerOpts("-L/usr/lib/x86_64-linux-gnu", "-Llib/linuxX64", "-lSDL2", "-llvgl", "-llv_drivers")
            }
        }
    }

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            cinterops.create("lvgl") {
                val homeDir = System.getProperty("user.home") ?: ""
                val lvglVer = "8.3.3"
                includeDirs("$homeDir/c_libs/lvgl/lvgl-$lvglVer")
            }
            dependencies {
                implementation("io.gitlab.embed-soft:lvglkt-core:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-frame-buffer:$lvglKtVer")
                implementation(coreWidgetsDependency)
            }
        }

        binaries {
            executable(binaryName) {
                entryPoint = programEntryPoint
                linkerOpts(
                    "--library-path=lib/linuxArm32",
                    "--library=lvgl",
                    "--library=lv_drivers"
                )
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.7.21"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("$embedSoftGroupId:lvglkt-core:$lvglKtVer")
                implementation("$embedSoftGroupId:lvglkt-drivers:$lvglKtVer")
                implementation(coreWidgetsDependency)
            }
        }
    }
}
